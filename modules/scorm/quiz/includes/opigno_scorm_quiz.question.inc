<?php

/**
 * @file
 * Question type class definition.
 */

class OpignoScormQuizQuestion extends QuizQuestion {

  /**
   * @copydoc QuizQuestion::validateNode()
   */
  public function validateNode(array &$form) {
    $file = file_load($form['opigno_scorm_package']['und'][0]['#default_value']['fid']);
    if ($file && $file->status != FILE_STATUS_PERMANENT) {
      $path = drupal_realpath($file->uri);
      $validate = _opigno_package_files_validate($path);
      if ($validate['error']) {
        watchdog('opigno_scorm', $validate['message'], array(), WATCHDOG_ERROR);
        form_set_error('opigno_scorm_package', t('The package was not extracted for security reasons.'));
        drupal_set_message($validate['message'], 'error');
      }
    }
  }

  /**
   * @copydoc QuizQuestion::getCreationForm()
   */
  public function getCreationForm(array &$form_state = NULL) {
    // No special fields.
    return array();
  }

  /**
   * @copydoc QuizQuestion::getAnsweringForm()
   */
  public function getAnsweringForm(array $form_state = NULL, $rid) {
    return array(
      'tries' => array(
        '#type' => 'hidden',
        '#default_value' => 1,
      ),
    );
  }

  /**
   * @copydoc QuizQuestion::getMaximumScore()
   */
  public function getMaximumScore() {
    return variable_get('opigno_scorm_quiz_max_score', 50);
  }

  /**
   * @copydoc QuizQuestion::saveNodeProperties()
   */
  public function saveNodeProperties($is_new = FALSE) {
    // No properties to save.
  }

}
